# My code projects have moved - information on where to find them now #

Bitbucket removed their Mercurial support in 2020, and I migrated all my projects (except dreamer, an abandoned project with barely anything in it) off Bitbucket.

The current home of my code projects is:

https://heptapod.host/jp-lebreton

If that ever changes, I will make sure to update this document here.